//
//  MasterViewController.h
//  ProjectManagement-Companion
//
//  Created by Lars Günzel on 13.06.13.
//  Copyright (c) 2013 Lars Günzel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;

@end
